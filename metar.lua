#!/usr/bin/env lua

local tokens = {
  { -- ICAO code
    patterns = {'^(%u%u%u%u)$'},
    handler = function (s) return {icao_code=s}; end,
  },
  { -- Observer timestamp
    patterns = {'^(%d%d)(%d%d)(%d%d)Z$'},
    handler = function (day, hour, min)
      local timestamp = os.date("!*t")

      day = tonumber(day)
      if timestamp.day + day < day * 2 then
        timestamp.month = timestamp.month - 1
        while timestamp.month < 1 do
          timestamp.month = timestamp.month + 12
          timestamp.year = timestamp.year - 1
        end
      end

      timestamp.day = day
      timestamp.hour = tonumber(hour)
      timestamp.min = tonumber(min)
      timestamp.sec = 0
      return {timestamp = os.time(timestamp)}
    end,
  },
  { -- Wind
    patterns = {'^(%d%d%d)(P?%d%d)(G%d%d)(%u%u+)$',
                '^(%d%d%d)(P?%d%d)(%u%u+)$',
                '^(VRB)(P?%d%d)(G%d%d)(%u%u+)$',
                '^(VRB)(P?%d%d)(%u%u+)$'},
    handler = function (dir, speed, gust, unit)
      if dir ~= "VRB" then
        dir = tonumber(dir)
      end

      if speed:match("^P") then
        speed = -1
      else
        speed = tonumber(speed)
      end

      if gust:match("^G") then
        gust = tonumber(gust:sub(2))
      else
        unit = gust
        gust = nil
      end

      if speed > 0 then
        if unit == "KMH" then
          speed = math.floor(speed / 0.36 + 0.5) / 10
          if gust then gust = math.floor(gust / 0.36 + 0.5) / 10; end
        elseif unit == "KT" then
          speed = math.floor(speed * 18.52 / 3.6 + 0.5) / 10
          if gust then gust = math.floor(gust * 18.52 / 3.6 + 0.5) / 10; end
        end
      end

      return {wind = {direction_degree = dir, speed_mps = speed, gust_mps = gust}}
    end,
  },
  { -- Variable Wind
    patterns = {'^(%d%d%d)V(%d%d%d)$'},
    handler = function (min_dir, max_dir)
      return {wind_direction_variable = {min_degree = min_dir, max_degree = max_dir}}
    end,
  },
  { -- Visibility
    patterns = {'^(%d%d%d%d)(%u*)$'},
    handler = function (visibility, unit)
      visibility = tonumber(visibility, 10)
      if visibility == 9999 then
        visibility = -1
      elseif unit == "SM" then
        visibility = visibility * 1852
      end
      return {visibility_m = visibility}
    end,
  },
  { -- Weather
    patterns = {'^(VC)(%u%u)(%u%u)$', '^(VC)(%u%u)$',
                '^([+-]?)(%u%u)(%u%u)$', '^([+-]?)(%u%u)$',
                '^(NSW)$'},
    handler = function (intensity, descriptor, phenomena)
      if intensity == "NSW" then
        phenomena = "NSW"
        intensity = nil
      elseif intensity and #intensity == 0 then
        intensity = nil
      end
      if not phenomena then
        phenomena = descriptor
        descriptor = nil
      end
      return {weather = {intensity=intensity, descriptor=descriptor, phenomena=phenomena}}
    end,
  },
  { -- Clouds
    patterns = {'^(CAVOK)$', '^(%u%u%u)(%d%d%d)(%u*)$', '^(VV)(%d%d%d)$','^(NSC)$'},
    handler = function (coverage, altitude, type)
      if coverage == "CAVOK" then
        return {visibility_m = -1, clouds = {coverage=coverage}}
      elseif altitude then
        altitude = math.floor(tonumber(altitude) * 30.48 + 0.5)
      end
      if type and #type == 0 then
        type = nil
      end
      return {clouds = {coverage=coverage, altitude_m=altitude, type=type}}
    end,
  },
  { -- Temperature and dew point
    patterns = {'^(M?%d%d)/(M?%d%d)$'},
    handler = function (t, dp)
      t = tonumber(({t:gsub('^M', '-'):gsub('^(-?)0+', '%1')})[1])
      dp = tonumber(({dp:gsub('^M', '-'):gsub('^(-?)0+', '%1')})[1])
      return {temperature_Celsius = t, dewpoint_Celsius = dp}
    end,
  },
  { -- Sea level pressure
    patterns = {'^([QA])(%d%d%d%d)$'},
    handler = function (unit, pressure)
      if unit == 'A' then
        pressure = math.floor(tonumber(pressure) * 0.3386389 + 0.5)
      else
        pressure = tonumber(pressure)
      end
      return { pressure_hPa = pressure }
    end,
  },
}

local function parse_metar(s, output_fn)
  local c = 0
  local matched
  local first_token_to_match, token = next(tokens)
  local i_token = first_token_to_match

  for word in s:gmatch("%S+") do
    matched = false

    repeat
      for _, pattern in ipairs(token.patterns) do
        if word:match(pattern) then
          matched = true
          first_token_to_match = i_token
          c = c + 1
          output_fn(token.handler(word:match(pattern)))
          break
        end -- end if match
      end -- for each pattern

      if not matched then
        i_token, token = next(tokens, i_token)
      else
        break
      end
    until i_token == nil

    i_token, token = first_token_to_match, tokens[first_token_to_match]
  end -- for each word

  return c
end

local dictionary_codes = {
  -- Descritor
  BC = "fragmentado(a)",
  BL = "soprado(a)",
  DR = "flutuante",
  FZ = "congelante",
  MI = "raso(a)",
  PR = "parcial",
  SH = "em pancada",
  TS = "com trovoada",

  -- Precipitação
  DZ = "Chuvisco",
  GR = "Granizos",
  GS = "Granizos pequenos",
  IC = "Cristais de gelo",
  PL = "Pelotas de gelo",
  RA = "Chuva",
  SG = "Grãos de neve",
  SN = "Neve",
  UP = "Precip. desconhec.",

  -- Obscurecedor
  BR = "Névoa úmida",
  DU = "Poeira extensa",
  FG = "Nevoeiro",
  FU = "Fumaça",
  HZ = "Névoa seca",
  PY = "Maresia",
  SA = "Areia",
  VA = "Cinzas vulcânicas",

  -- Outros
  DS = "Tempestade de poeira",
  FC = "Nuvens em funil",
  PO = "Poeira/areia em redemoninhos",
  SQ = "Tempestade",
  SS = "Tempestade de areia",

  -- Intensidade
  ['-'] = "leve",
  ['+'] = "pesado(a)",
  VC = "nas vizinhanças",

  -- Nuvens
  OVC = "Encoberto",
  BKN = "Muitas (¾)",
  SCT = "Parcial (½)",
  FEW = "Poucas (¼)",
  CB = "Cumulonimbus",
  TCU = "CN em formação",
  VV = "Visib. vert.",
  NSC = "Sem nuvens",
  CAVOK = "Sem nuvens",
}

local function to_compass_points(degree)
  local compass_points = {"N", "NNE", "NE", "ENE", "E", "ESE", "SE", "SSE",
                          "S", "SSW", "SW", "WSW", "W", "WNW", "NW", "NNW"}
  return compass_points[math.floor((degree + 11.25) / 22.5) % #compass_points + 1]
end

local formatters_fn = {
  icao_code = function (v) return "Código ICAO", v; end,

  timestamp = function (v)
    return "Observação em", os.date("Dia %d, %H:%M UTC", v)
  end,

  wind = function (v)
    local s
    if v.speed_mps == 0 then
      s = "Calmo"
    else
      if not v.direction_degree or v.direction_degree == "VRB" then
        s = "Dir. variável"
      else
        s = string.format("%s (%i°)",
                          to_compass_points(v.direction_degree),
                          v.direction_degree)
      end

      if v.gust_mps then
        s = s .. string.format(" @ (%.1f a %.1f) m/s",
                        math.floor(v.speed_mps * 2 + 0.5) / 2,
                        math.floor(v.gust_mps * 2 + 0.5) / 2)
      elseif v.speed_mps < 0 then
        s = s .. " @ > 100"
      else
        s = s .. string.format(" @ %.1f m/s",
                        math.floor(v.speed_mps * 2 + 0.5) / 2)
      end
    end
    return "Vento", s
  end,

  wind_direction_variable = function (v)
    return "Dir. dos ventos", string.format("Entre %s e %s (%i° a %i°)",
                                            to_compass_points(v.min_degree),
                                            to_compass_points(v.max_degree),
                                            v.min_degree, v.max_degree)
  end,

  visibility_m = function (v)
    if v < 0 then
      v = "Boa"
    elseif v < 2000 then
      v = string.format("%i m", v)
    else
      v = string.format("%.1f km", v / 1000)
    end
    return "Visibilidade", v
  end,

  weather = function (v)
    local s

    if v.phenomena and dictionary_codes[v.phenomena] and
      (not v.descriptor or dictionary_codes[v.descriptor]) and
      (not v.intensity or dictionary_codes[v.intensity])
    then
      s = dictionary_codes[v.phenomena]
      if v.descriptor then
        s = s .. " " .. dictionary_codes[v.descriptor]
      end
      if v.intensity then
        s = s .. ", " .. dictionary_codes[v.intensity]
      end
    else
      s = (v.intensity and v.intensity .. " " or "") ..
          (v.descriptor and v.descriptor .. " " or "") ..
          v.phenomena
    end

    return "Evento", s
  end,

  clouds = function (v)
    if v.altitude_m then
      local alt_str
      if v.altitude_m < 2000 then
        alt_str = string.format("%i m", math.floor(v.altitude_m / 10 + 0.5) * 10)
      else
        alt_str = string.format("%.1f km", v.altitude_m / 1000)
      end

      if v.type then
        v = string.format("%s de %s @ %s",
                          dictionary_codes[v.coverage] or v.coverage,
                          dictionary_codes[v.type] or v.type,
                          alt_str)
      else
        v = string.format("%s @ %s",
                          dictionary_codes[v.coverage] or v.coverage,
                          alt_str)
      end
      return "Nuvens", v
    end

    return "Nuvens", dictionary_codes[v.coverage] or v.coverage
  end,

  temperature_Celsius = function (v)
    return "Temperatura", string.format("%i °C", v)
  end,
  dewpoint_Celsius = function (v)
    return "Ponto de orvalho", string.format("%i °C", v)
  end,
  relative_humidity = function (v)
    return "Umidade relativa", string.format("%i %%", v)
  end,
  heat_index = function (v)
    return "Índice de calor", string.format("%i °C", math.floor(v + 0.5))
  end,
  pressure_hPa = function (v)
    return "Pressão atmosf.", string.format("%.1f kPa", v / 10)
  end,
}

local function print_decoded_metar(t)
  for k, v in pairs(t) do
    if formatters_fn[k] then
      print(string.format(" - %-16s:\t%24s", formatters_fn[k](v)))
    elseif type(v) == "table" then
      for j, x in pairs(v) do
        print(string.format(" - %-16s:\t%24s", k .. "." .. j, x))
      end
    else
      print(string.format(" - %-16s:\t%24s", k, v))
    end
  end
  if t.temperature_Celsius then
    local function rh_from_dewpoint(t, tdp)
      if tdp < t then
        local A, B, C = 6.1121, 17.368, 238.88
        -- Ref.: <http://www.paroscientific.com/dewpoint.htm>;
        --       <http://en.wikipedia.org/wiki/Dew_point>.
        return math.floor(100 * (A * math.exp(B * tdp / (tdp + C)))
                              / (A * math.exp(B * t / (t + C)))    + 0.5)
      else
        return 100
      end
    end

    local function heatindexof(t, rh)
      -- Ref.: <heatIndex.pdf>, by <tim.brice>.

      if t < 27 then return t; end

      local rhrh, tt = rh * rh, t * t

      tt = (- 3.582E-6 * tt * rhrh
            + 7.2546E-4 * t * rhrh
            - 0.016424827778 * rhrh
            + 0.002211732 * tt * rh
            - 0.14611605 * t * rh
            + 2.338548838888 * rh
            - 0.012308094 * tt
            + 1.61139411 * t
            - 8.784694755556)

      return tt > t and tt or t
    end

    local rh = rh_from_dewpoint(t.temperature_Celsius, t.dewpoint_Celsius)
    print(string.format(" - %-16s:\t%24s", formatters_fn.relative_humidity(rh)))

    rh = heatindexof(t.temperature_Celsius, rh)
    if rh > t.temperature_Celsius then
      print(string.format(" - %-16s:\t%24s", formatters_fn.heat_index(rh)))
    end
  end
end

os.setlocale("")
for line in io.lines() do
  print(line)
  if line:gsub("%s","") ~= "" then
    if parse_metar(line, print_decoded_metar) == 0 then
      io.stderr:write("Not a METAR format!\n")
    end
    print()
  end
end
