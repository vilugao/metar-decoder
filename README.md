﻿Leitor de código METAR
======================

Este script escrito em lua lê, na entrada padrão, em cada linha, os códigos
[METAR](http://en.wikipedia.org/wiki/METAR) e gera na saída padrão os
respectivos códigos decodificados.

Amostra de saída do programa
----------------------------

    METAR SBAT 021800Z 09007KT 9999 SCT030 38/20 Q1008
     - Código ICAO     :                               SBAT
     - Observação em   :                  Dia 02, 18:00 UTC
     - Vento           :                  E (90°) @ 3,5 m/s
     - Visibilidade    :                                Boa
     - Nuvens          :                Parcial (½) @ 910 m
     - Temperatura     :                              38 °C
     - Ponto de orvalho:                              20 °C
     - Umidade relativa:                               35 %
     - Índice de calor :                              41 °C
     - Pressão atmosf. :                          100,8 kPa
    
    SBCT 031200Z 16005G15KT 0012 -GS BR OVC020TCU M01/M05 Q1021
     - Código ICAO     :                               SBCT
     - Observação em   :                  Dia 03, 12:00 UTC
     - Vento           :       SSE (160°) @ (2,5 a 7,5) m/s
     - Visibilidade    :                               12 m
     - Evento          :            Granizos pequenos, leve
     - Evento          :                        Névoa úmida
     - Nuvens          :Encoberto de CN em formação @ 610 m
     - Temperatura     :                              -1 °C
     - Ponto de orvalho:                              -5 °C
     - Umidade relativa:                               74 %
     - Pressão atmosf. :                          102,1 kPa
    
    METAR LBBG 041600Z 12003MPS 310V290 1400 R04/P1500N R22/P1500U +SN BKN022 OVC050 M04/M07 Q1020 NOSIG 9949//91=
     - Código ICAO     :                               LBBG
     - Observação em   :                  Dia 04, 16:00 UTC
     - Vento           :               ESE (120°) @ 3,0 m/s
     - Dir. dos ventos :       Entre NW e WNW (310° a 290°)
     - Visibilidade    :                             1400 m
     - Evento          :                    Neve, pesado(a)
     - Nuvens          :                 Muitas (¾) @ 670 m
     - Nuvens          :                 Encoberto @ 1520 m
     - Temperatura     :                              -4 °C
     - Ponto de orvalho:                              -7 °C
     - Umidade relativa:                               80 %
     - Pressão atmosf. :                          102,0 kPa

Como usar
---------

Você pode colar os dados METAR no console terminal com o script em execução.
Para ter informações atualizadas diretamente, a sugestão é usar `wget` ou
`cURL` e usando o pipe `|`. Se você é usuário do Windows, o comando `ftp`
é uma alternativa. Consulte seu manual para mais orientações.

Exemplo:

    curl http://site-meteorologico.com/metar/SBBR.TXT | lua metar.lua
